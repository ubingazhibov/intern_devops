FROM ubuntu:20.04
RUN apt-get update && apt install nginx -y
WORKDIR /home/aslan
COPY ["index.html", "nginx.conf", "run_docker.sh", "./"]

RUN ./run_docker.sh
CMD ["nginx", "-g", "daemon off;"]

#docker build -t hardcode .
#docker run -p 80:80 hardcode

