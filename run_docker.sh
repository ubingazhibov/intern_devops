#! /bin/bash
set -xe


cp -rf ./nginx.conf /etc/nginx/ # копируем наш nginx файл в директорию nginx

useradd --no-create-home --shell /sbin/nologin www-data||true # создадим юзера для nginx, но насколько я знаю он создается автоматический при скачивании nginx. Оставим команду на всякий случае
mkdir /opt/www-data||true # создадим директорию, для html файла
cp -rf ./index.html /opt/www-data/ # скопируем html с hello world на /opt/www-data так как nginx там будет искать index.html
chown -R www-data:www-data /opt/www-data/ # поменям оунера на www-data

nginx -t #проверям все ли в порядке с конфигами nginx
