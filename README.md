# Задача 1

1. Скачал nginx
   - sudo apt-get update
   - sudo apt install nginx

2. Создал nginx конфиг файл - **nginx.conf**
3. Создал html файл - **index.html**. Скопировал [отсюда](https://www.geeksforgeeks.org/html-course-first-web-page-printing-hello-world/)
4. Остальное в **run.sh**, где он ранит nginx с новым конфиг файлом и с index.html.
5. **gitignore** взял [отсюда](https://github.com/github/gitignore/blob/main/Python.gitignore)

# Задача 2
1. Создал docker файл, где он 
   - скачивает образ [убунту 20.04](https://hub.docker.com/_/ubuntu) 
   - скачивает nginx
   - копирует index.html, nginx.conf, run_docker.sh в рабочую директорию в докере
   - запускает **run_docker.sh**
   - запускает nginx
2. **run_docker.sh** немного отличается от **run.sh** 
 - запускаю все без sudo, да, можно было бы скачать sudo(apt-get -y install sudo), но он мне впринципе был не нужен
 - nginx запускается не через systemctl, так как в докере этого нет.
3. Запускаем docker:
- docker build -t hardcode . 
- docker run -p 80:80 hardcode 

# Проблемы, с которыми я столкнулся
На самом деле, проблем особо не было, но: <br> <br>
Cначала я докер файл сделал с помощью образа самого [nginx](https://hub.docker.com/_/nginx), что очень сильно облягчало работу.
   **Dockerfile** выглядил бы так
```
FROM nginx

COPY ./index.html /usr/share/nginx/html
```
Но как я понял, вы хотели увидеть как это делается с образом дистрибутива Linux, где я не сразу понял как запускать nginx внутри докер image.
Так sudo systemctl reload nginx, /etc/init.d/nginx start [не работают](https://stackoverflow.com/questions/74823139/systemctl-command-doesnt-work-on-centos-with-docker) в докере.
# Автор
 Аслан Убингажибов - aslan.ubingazhibov@alumni.nu.edu.kz, telegram - https://t.me/ubingazhibov