#! /bin/bash
set -xe


sudo cp -rf ./nginx.conf /etc/nginx/ # копируем наш nginx файл в директорию nginx

sudo useradd --no-create-home --shell /sbin/nologin www-data||true # создадим юзера для nginx, но насколько я знаю он создается автоматический при скачивании nginx. Оставим команду на всякий случае
sudo mkdir /opt/www-data||true # создадим директорию, для html файла
sudo cp -rf ./index.html /opt/www-data/ # скопируем html с hello world на /opt/www-data так как nginx там будет искать index.html
sudo chown -R www-data:www-data /opt/www-data/ # поменям оунера на www-data

sudo nginx -t #проверям все ли в порядке с конфигами nginx
sudo systemctl daemon-reload
sudo systemctl start nginx #если nginx не включен
sudo systemctl reload nginx # если nginx включен
#проверьте http://localhost:80/